package ssm.view;

import java.awt.event.MouseAdapter;
import java.io.File;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import properties_manager.PropertiesManager;
import ssm.LanguagePropertyType;
import static ssm.LanguagePropertyType.ERROR_DATA_FILE_LOADING;
import static ssm.StartupConstants.CSS_CLASS_SLIDE_EDIT_VIEW;
import static ssm.StartupConstants.CSS_CLASS_SLIDE_SELECT_VIEW;
import static ssm.StartupConstants.DEFAULT_THUMBNAIL_WIDTH;
import ssm.controller.ImageSelectionController;
import ssm.error.ErrorHandler;
import ssm.model.Slide;
import static ssm.file.SlideShowFileManager.SLASH;
import ssm.model.SlideShowModel;

/**
 * This UI component has the controls for editing a single slide
 * in a slide show, including controls for selected the slide image
 * and changing its caption.
 * 
 * @author McKilla Gorilla & Ahsan Qureshi
 */
public class SlideEditView extends HBox {
    // SLIDE THIS COMPONENT EDITS
    Slide slide;
    Boolean eWasThrown;
    
    // DISPLAYS THE IMAGE FOR THIS SLIDE
    ImageView imageSelectionView;
    
    // CONTROLS FOR EDITING THE CAPTION
    VBox captionVBox;
    Label captionLabel;
    TextField captionTextField;
    
    // PROVIDES RESPONSES FOR IMAGE SELECTION
    ImageSelectionController imageController;
    
    SlideShowMakerView uin;

    /**
     * THis constructor initializes the full UI for this component, using
     * the initSlide data for initializing values./
     * 
     * @param initSlide The slide to be edited by this component.
     */
    public SlideEditView(Slide initSlide, SlideShowMakerView ui, Boolean isSelected, TextField initTF) {
	
        //Get slide show model to add selected slide
        SlideShowModel ssm = ui.getSlideShow();
        uin = ui;
        eWasThrown = false;
        // FIRST SELECT THE CSS STYLE CLASS FOR THIS CONTAINER
	if(isSelected) {
            this.getStyleClass().add(CSS_CLASS_SLIDE_SELECT_VIEW);
        }
        else {
            this.getStyleClass().add(CSS_CLASS_SLIDE_EDIT_VIEW);
        }
	
	// KEEP THE SLIDE FOR LATER
	slide = initSlide;
	
	// MAKE SURE WE ARE DISPLAYING THE PROPER IMAGE
	imageSelectionView = new ImageView();
	try {
            updateSlideImage();
        }
        catch (Exception E) {
            eWasThrown = true;
        }
        
	// SETUP THE CAPTION CONTROLS
	captionVBox = new VBox();
	PropertiesManager props = PropertiesManager.getPropertiesManager();
	captionLabel = new Label(props.getProperty(LanguagePropertyType.LABEL_CAPTION));
	captionTextField = initTF;
	captionVBox.getChildren().add(captionLabel);
	captionVBox.getChildren().add(captionTextField);

	// LAY EVERYTHING OUT INSIDE THIS COMPONENT
	getChildren().add(imageSelectionView);
	getChildren().add(captionVBox);
        
        captionTextField.setOnMousePressed(e -> {
           ssm.setSelectedSlide(slide);
            try {
                ui.reloadSlideShowPane(ssm);
            } catch (Exception ex) {
            }
           ui.setSlideButtonsDisable(false);
           //captionTextField
        });
	// SETUP THE EVENT HANDLERS
        this.setOnMousePressed(e -> {
            ssm.setSelectedSlide(slide);
            try {
                ui.reloadSlideShowPane(ssm);
            } catch (Exception ex) {
            }
            ui.setSlideButtonsDisable(false);
        });
	imageController = new ImageSelectionController();
	imageSelectionView.setOnMousePressed(e -> {
            ssm.setSelectedSlide(slide);
            ui.setSlideButtonsDisable(false);
            try {
                ui.reloadSlideShowPane(ssm);
            } catch (Exception ex) {
            }
	    imageController.processSelectImage(slide, this);
	});
        captionTextField.setOnAction(e -> {
            slide.setCaption(captionTextField.getText());
            ui.updateToolbarControls(false);
        });
    }
    
    public Boolean eThrown() {
        return eWasThrown;
    }
    
    /**
     * This function gets the image for the slide and uses it to
     * update the image displayed.
     */
    public void updateSlideImage() throws Exception {
	String imagePath = slide.getImagePath() + SLASH + slide.getImageFileName();
	File file = new File(imagePath);
	    // GET AND SET THE IMAGE
        URL fileURL;
        try{
            fileURL = file.toURI().toURL();
        }
        catch (Exception e){
            return;
        }
	    Image slideImage = new Image(fileURL.toExternalForm());
            if(slideImage.isError())
                throw slideImage.getException();
                    
            imageSelectionView.setImage(slideImage);
	    
	    // AND RESIZE IT
	    double scaledWidth = DEFAULT_THUMBNAIL_WIDTH;
	    double perc = scaledWidth / slideImage.getWidth();
	    double scaledHeight = slideImage.getHeight() * perc;
	    imageSelectionView.setFitWidth(scaledWidth);
	    imageSelectionView.setFitHeight(scaledHeight);       
    }
}