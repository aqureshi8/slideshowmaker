/* 
 * Javascript file to control the slideshow.
 */

var caps = [$caps];
var imgs = [$imgs];
var counter = 0;
var myVar = setInterval(slideTimer, 5000); 

function slideTimer() {
    var playpausebutton = document.getElementById("playpausebutton");
    if(playpausebutton.value === "b") {
        nextSlide();
    }
}

function playpause() {
    var playpausebutton = document.getElementById("playpausebutton");
    if(playpausebutton.value === "a") {
        playpausebutton.innerHTML = "&#10074&#10074";
        playpausebutton.value = "b";
    }
    else {
        playpausebutton.innerHTML = "&#9654";
        playpausebutton.value = "a";
    }
}

function prevSlide() {
    
    if(counter === 0) {
        counter = caps.length - 1;
    }
    else {
        counter--;
    }
    
    var caption = document.getElementById("caption");
    var pic = document.getElementById("pictodisp");
    
    caption.innerHTML = caps[counter];
    pic.src = "img/" + imgs[counter];
}

function nextSlide() {
    
    counter++;
    counter = counter%caps.length;
    
    var caption = document.getElementById("caption");
    var pic = document.getElementById("pictodisp");
    
    caption.innerHTML = caps[counter];
    pic.src = "img/" + imgs[counter];
}