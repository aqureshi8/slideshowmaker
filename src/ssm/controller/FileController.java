package ssm.controller;

import com.sun.javaws.Main;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Optional;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonBar.ButtonData;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.FlowPane;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import static jdk.nashorn.internal.objects.NativeRegExp.source;
import properties_manager.PropertiesManager;
import static ssm.LanguagePropertyType.ERROR_DATA_FILE_LOADING;
import static ssm.LanguagePropertyType.ERROR_DATA_FILE_SAVING;
import static ssm.LanguagePropertyType.ERROR_EXIT;
import static ssm.LanguagePropertyType.ERROR_NEW_SLIDE_SHOW;
import static ssm.LanguagePropertyType.LABEL_TITLECHANGE;
import static ssm.LanguagePropertyType.SAVE_PROMPT;
import static ssm.LanguagePropertyType.SAVE_PROMPT_HEADER;
import static ssm.LanguagePropertyType.SAVE_PROMPT_TITLE;
import static ssm.LanguagePropertyType.SLIDE_SHOW_WINDOW;
import static ssm.LanguagePropertyType.SSALERT;
import static ssm.LanguagePropertyType.SSALERT_HEADER;
import static ssm.LanguagePropertyType.SSALERT_TITLE;
import static ssm.StartupConstants.DEFAULT_THUMBNAIL_WIDTH;
import static ssm.StartupConstants.PATH_SITES;
import ssm.model.SlideShowModel;
import ssm.error.ErrorHandler;
import ssm.file.SlideShowFileManager;
import ssm.view.SlideShowMakerView;
import static ssm.StartupConstants.PATH_SLIDE_SHOWS;
import static ssm.StartupConstants.STYLE_SHEET_UI;
import static ssm.file.SlideShowFileManager.SLASH;
import ssm.model.Slide;
/**
 * This class serves as the controller for all file toolbar operations,
 * driving the loading and saving of slide shows, among other things.
 * 
 * @author McKilla Gorilla & Ahsan Qureshi
 */
public class FileController {

    // WE WANT TO KEEP TRACK OF WHEN SOMETHING HAS NOT BEEN SAVED
    private boolean saved;
    private boolean tChange;
    private int currentSlide;

    // THE APP UI
    private SlideShowMakerView ui;
    
    // THIS GUY KNOWS HOW TO READ AND WRITE SLIDE SHOW DATA
    private SlideShowFileManager slideShowIO;

    /**
     *Changes the value of tChange to a given boolean
     * 
     * @param change the boolean value to be given to tChange
     */
    
    /**
     * This default constructor starts the program without a slide show file being
     * edited.
     *
     * @param initSlideShowIO The object that will be reading and writing slide show
     * data.
     */
   
    public FileController(SlideShowMakerView initUI, SlideShowFileManager initSlideShowIO) {
        // NOTHING YET
        saved = true;
	ui = initUI;
        slideShowIO = initSlideShowIO;
    }
    
    
    public void setTChange(Boolean change) {
        
        tChange = change;
        currentSlide = 0;
    }
   
    
    public void markAsEdited() {
        saved = false;
        ui.updateToolbarControls(saved);
    }

    /**
     * This method starts the process of editing a new slide show. If a pose is
     * already being edited, it will prompt the user to save it first.
     */
    public void handleNewSlideShowRequest() {
        try {
            // WE MAY HAVE TO SAVE CURRENT WORK
            boolean continueToMakeNew = true;
            if (!saved) {
                // THE USER CAN OPT OUT HERE WITH A CANCEL
                continueToMakeNew = promptToSave();
            }

            // IF THE USER REALLY WANTS TO MAKE A NEW COURSE
            if (continueToMakeNew) {
                // RESET THE DATA, WHICH SHOULD TRIGGER A RESET OF THE UI
                SlideShowModel slideShow = ui.getSlideShow();
		slideShow.reset();
                saved = false;

                // REFRESH THE GUI, WHICH WILL ENABLE AND DISABLE
                // THE APPROPRIATE CONTROLS
                ui.updateToolbarControls(saved);
                try {
                    ui.reloadSlideShowPane(slideShow);
                }
                catch(Exception e) {
                    
                }
                PropertiesManager props = PropertiesManager.getPropertiesManager();
                
                Alert ssAlert = new Alert(AlertType.INFORMATION);
                ssAlert.setTitle(props.getProperty(SSALERT_TITLE));
                ssAlert.setHeaderText(props.getProperty(SSALERT_HEADER));
                ssAlert.setContentText(props.getProperty(SSALERT));
                ssAlert.showAndWait();
                // TELL THE USER THE SLIDE SHOW HAS BEEN CREATED
                // @todo
            }
        } catch (IOException ioe) {
            ErrorHandler eH = ui.getErrorHandler();
            eH.processError(ERROR_NEW_SLIDE_SHOW, "Error", "Message");
        }
    }

    /**
     * This method lets the user open a slideshow saved to a file. It will also
     * make sure data for the current slideshow is not lost.
     */
    public void handleLoadSlideShowRequest() {
        try {
            // WE MAY HAVE TO SAVE CURRENT WORK
            boolean continueToOpen = true;
            if (!saved) {
                // THE USER CAN OPT OUT HERE WITH A CANCEL
                continueToOpen = promptToSave();
            }

            // IF THE USER REALLY WANTS TO OPEN A POSE
            if (continueToOpen) {
                // GO AHEAD AND PROCEED MAKING A NEW POSE
                promptToOpen();
            }
        } catch (IOException ioe) {
            ErrorHandler eH = ui.getErrorHandler();
            eH.processError(ERROR_DATA_FILE_LOADING, "Error", "Message");//@todo provide error message
        }
    }

    /**
     * This method will save the current slideshow to a file. Note that we already
     * know the name of the file, so we won't need to prompt the user.
     */
    public boolean handleSaveSlideShowRequest() {
        try {
	    // GET THE SLIDE SHOW TO SAVE
	    SlideShowModel slideShowToSave = ui.getSlideShow();
	    
            // SAVE IT TO A FILE
            slideShowIO.saveSlideShow(slideShowToSave);

            // MARK IT AS SAVED
            saved = true;

            // AND REFRESH THE GUI, WHICH WILL ENABLE AND DISABLE
            // THE APPROPRIATE CONTROLS
            ui.updateToolbarControls(saved);
	    return true;
        } catch (IOException ioe) {
            ErrorHandler eH = ui.getErrorHandler();
            eH.processError(ERROR_DATA_FILE_SAVING, "Error", "Message");
	    return false;
        }
    }
    
    /**
     * This method prompts the user to enter a title for their slideshow and saves that name
     */
    
    public void handleChangeTitleRequest() {
                    
            PropertiesManager props = PropertiesManager.getPropertiesManager();

            //Get The slideshow
            SlideShowModel slideShowForNewTitle = ui.getSlideShow();
            
            //Setup prompt stage for user to change title
            Stage newTitleStage = new Stage();
            
            //Initialize a button and a textfield
            TextField title = new TextField(slideShowForNewTitle.getTitle());
            Button okButton = new Button("OK");
            
            okButton.setOnAction(e -> {
                setTChange(true);
                newTitleStage.close();
            });
            
            //Setup a pane with a label asking for a title, a textfield, and a button
            FlowPane titlePane = new FlowPane();
            titlePane.getStylesheets().add(STYLE_SHEET_UI);
            titlePane.setPadding(new Insets(10,10,10,10));
            titlePane.getChildren().addAll(new Label(props.getProperty(LABEL_TITLECHANGE)), title, okButton);
            titlePane.setHgap(5);
            titlePane.setVgap(5);
            
            //Create the Scene
            Scene titleScene = new Scene(titlePane,250,75);
            
            //Setup Stage
            newTitleStage.setTitle("Title Change");
            newTitleStage.setScene(titleScene);
            newTitleStage.showAndWait();
            
            if(tChange == true) {
                slideShowForNewTitle.setTitle(title.getText());
                setTChange(false);
                saved = false;
                ui.updateToolbarControls(saved);

            }
    }

    public void handleViewSlideShowRequest() {
        
        File sitesFile = new File(PATH_SITES);
        File siteFile = new File(PATH_SITES + ui.getSlideShow().getTitle());
        
        if(!sitesFile.exists() || !sitesFile.isDirectory()) {
            sitesFile.mkdir();
        }
        
        //Setup the directories if they do not exist
        if(!siteFile.exists() || !siteFile.isDirectory()) {
           siteFile.mkdir();
            new File(PATH_SITES + ui.getSlideShow().getTitle() + "/css").mkdir();
            new File(PATH_SITES + ui.getSlideShow().getTitle() + "/js").mkdir();
            new File(PATH_SITES + ui.getSlideShow().getTitle() + "/img").mkdir();
            getCSS();

        }
        
        getImages();
        //writeJSON();
        writeJS();
        writeHTML();
        
        //Create Stage
        Stage slideViewStage = new Stage();
        
        //Get HTML File
        File htmlFile = new File(PATH_SITES + ui.getSlideShow().getTitle() + "/index.html");
        
        //Create a webview
        WebView wv = new WebView();
        WebEngine we = wv.getEngine();
        
        try {
            we.load(htmlFile.toURI().toURL().toString());
        }
        catch(Exception e) {}
        
        //Create Scene
        Scene slideViewScene = new Scene(wv, 1000, 714);
        
        //show stage
        slideViewStage.setScene(slideViewScene);
        slideViewStage.show();
        
        /* 
            //Get The slideshow
            SlideShowModel slideShowView = ui.getSlideShow();
            ObservableList<Slide> slides = slideShowView.getSlides();
            
            //Setup prompt stage for user to change title
            Stage viewStage = new Stage();
            
            //Setup pane
            BorderPane slideViewer = new BorderPane();
            slideViewer.setPadding(new Insets(10,10,10,10));
            
            //initialize buttons for slideshow viewing
            Label caption = new Label();
            Label title = new Label(slideShowView.getTitle());
            Button nextButton = new Button(">");
            Button prevButton = new Button("<");
            ImageView slidePic = null;
            
            reloadSlideShowView(caption, title, nextButton, prevButton, slidePic, currentSlide, slideViewer);
            
            //Button functions
            nextButton.setOnAction(e -> {
                incrementCurrentSlide();
                reloadSlideShowView(caption, title, nextButton, prevButton, slidePic, currentSlide, slideViewer);
            });
            
            prevButton.setOnAction(e -> {
               decrementCurrentSlide();
               reloadSlideShowView(caption, title, nextButton, prevButton, slidePic, currentSlide, slideViewer);
            });
            //Create the scene
            Sce1ne viewScene = new Scene(slideViewer, 1200,750);
            PropertiesManager props = PropertiesManager.getPropertiesManager();

            //Setup Stage
            viewStage.setTitle(props.getProperty(SLIDE_SHOW_WINDOW));
            viewStage.setScene(viewScene);
            viewStage.showAndWait();
        */
            
    }
    
    /*public void writeJSON() {
        //get template of json file
        File tempfile = new File(PATH_SLIDE_SHOWS + ui.getSlideShow().getTitle());
        String tempjs = "";
        
        //write a js file in temphtml
        try {
            Scanner s = new Scanner(tempfile);
            
            while(s.hasNext()){
                tempjs += s.nextLine();
            }
        }         
        catch (Exception ex) {
            System.out.println(ex);
        }
        
        try {
            BufferedWriter writer = new BufferedWriter(new FileWriter(PATH_SITES + ui.getSlideShow().getTitle() + "/slideshowjson.txt"));
            writer.write(tempjs);
            writer.flush();
            writer.close();
        } catch (Exception e) {
            System.out.println(e);
        }
    }*/
    
    public void getImages() {
        
        SlideShowModel slideShowToView = ui.getSlideShow();
            for (Slide slide : slideShowToView.getSlides()) {
                File imageFile = new File(slide.getImagePath() + SLASH + slide.getImageFileName());
                File imageDest = new File( PATH_SITES + ui.getSlideShow().getTitle() + "/img/" + slide.getImageFileName());
                try {
                    Files.copy(imageFile.toPath(), imageDest.toPath());
                } catch(Exception e) {
                }

            }
    }
   
    public void getCSS() {
        File cssSrc = new File("./src/ssm/controller/SlideShowSiteStyleTemplate.css");
        File cssDest = new File(PATH_SITES + ui.getSlideShow().getTitle() + "/css/SlideShowSiteStyle.css");
        try {
            Files.copy(cssSrc.toPath(),cssDest.toPath());
        }
        catch(Exception e) {
            System.out.println(e);
        }
    }
    
    public void writeJS() {
        //get template of js file
        File tempfile = new File("./src/ssm/controller/SlideShowScriptTemplate.js");
        String tempjs = "";
        
        //write a js file in temphtml
        try {
            Scanner s = new Scanner(tempfile);
            
            while(s.hasNext()){
                tempjs += s.nextLine();
            }
        }         
        catch (Exception ex) {
            System.out.println(ex);
        }
        
        tempjs = tempjs.replace("$caps", getCaps());
        tempjs = tempjs.replace("$imgs", getImgFiles());
        
        try {
            BufferedWriter writer = new BufferedWriter(new FileWriter(PATH_SITES + ui.getSlideShow().getTitle() + "/js/SlideShowScript.js"));
            writer.write(tempjs);
            writer.flush();
            writer.close();
        } catch (Exception e) {
            System.out.println(e);
        }        
    }
    
    public String getImgFiles() {
        
        String imgFileString ="";
        SlideShowModel slideShowToView = ui.getSlideShow();
        for (Slide slide : slideShowToView.getSlides()) {
            imgFileString += '"' + slide.getImageFileName() + '"';
            imgFileString += ", ";
        }
        imgFileString = imgFileString.substring(0,imgFileString.length() - 2);
        return imgFileString;
    }
    
    public String getCaps() {
        
        String capString = "";
        SlideShowModel slideShowToView = ui.getSlideShow();
        for (Slide slide : slideShowToView.getSlides()) {
            capString += '"' + slide.getCaption() + '"';
            capString += ", ";
        }
        capString = capString.substring(0,capString.length() - 2);
        return capString;
    }
    
    public void writeHTML() {
        
        File tempfile = new File("./src/ssm/controller/indextemplate.html");
        String temphtml = "";
        
        try {
            Scanner s = new Scanner(tempfile);
            
            while(s.hasNext()){
                temphtml += s.nextLine();
            }
        }         
        catch (Exception ex) {
            System.out.println(ex);
        }
        
        temphtml = temphtml.replace("$title", ui.getSlideShow().getTitle());
        temphtml = temphtml.replace("$imgfile", "img/" + ui.getSlideShow().getSlides().get(0).getImageFileName());
        temphtml = temphtml.replace("$caption", ui.getSlideShow().getSlides().get(0).getCaption());
        
        try {
            BufferedWriter writer = new BufferedWriter(new FileWriter(PATH_SITES + ui.getSlideShow().getTitle() + "/index.html"));
            writer.write(temphtml);
            writer.flush();
            writer.close();
        } catch (Exception e) {
            System.out.println(e);
        }
    }
    
    public void reloadSlideShowView(Label sCaption, 
                                    Label ssTitle, 
                                    Button nButton, 
                                    Button pButton,
                                    ImageView sPic,
                                    int slideNum,
                                    BorderPane pane) {    
            
        sPic = getImage(ui.getSlideShow().getSlides().get(slideNum));
        sCaption = new Label(ui.getSlideShow().getSlides().get(slideNum).getCaption());
       
        //setup title pane
        FlowPane northPane = new FlowPane();
        northPane.setAlignment(Pos.CENTER);
        northPane.getChildren().add(ssTitle);
      
        //setup pane for caption
        FlowPane southPane = new FlowPane();
        southPane.setAlignment(Pos.CENTER);
        southPane.getChildren().add(sCaption);
        
        //setup panes for buttons
        FlowPane eastPane, westPane;
        
        eastPane = new FlowPane();
        westPane = new FlowPane();
        
        eastPane.setAlignment(Pos.CENTER);
        westPane.setAlignment(Pos.CENTER);
        
        eastPane.getChildren().add(nButton);
        westPane.getChildren().add(pButton);
        
        //setup border pane
        pane.getChildren().clear();
        pane.setTop(northPane);
        pane.setBottom(southPane);
        pane.setCenter(sPic);
        pane.setLeft(westPane);
        pane.setRight(eastPane);
    }
    
    public ImageView getImage(Slide slide) {
         
        ImageView imageSelectionView = new ImageView();
        
        //getImage for slide
        String imagePath = slide.getImagePath() + SLASH + slide.getImageFileName();
        File file = new File(imagePath);
        try {
            // GET AND SET THE IMAGE
            URL fileURL = file.toURI().toURL();
            Image slideImage = new Image(fileURL.toExternalForm());
            imageSelectionView.setImage(slideImage);
	    
            // AND RESIZE IT
            double scaledWidth = 400;
            double perc = scaledWidth / slideImage.getWidth();
            double scaledHeight = slideImage.getHeight() * perc;
            imageSelectionView.setFitWidth(scaledWidth);
            imageSelectionView.setFitHeight(scaledHeight);
        } catch (Exception e) {
            ErrorHandler eH = ui.getErrorHandler();
            eH.processError(ERROR_DATA_FILE_LOADING, "Error", "Message");
	    return null;    
        // @todo - use Error handler to respond to missing image
        }
        return imageSelectionView;
    }
    
     /**
     * This method will exit the application, making sure the user doesn't lose
     * any data first.
     */
    public void handleExitRequest() {
        try {
            // WE MAY HAVE TO SAVE CURRENT WORK
            boolean continueToExit = true;
            if (!saved) {
                // THE USER CAN OPT OUT HERE
                continueToExit = promptToSave();
            }

            // IF THE USER REALLY WANTS TO EXIT THE APP
            if (continueToExit) {
                // EXIT THE APPLICATION
                System.exit(0);
            }
        } catch (IOException ioe) {
            ErrorHandler eH = ui.getErrorHandler();
            eH.processError(ERROR_EXIT, "Error", "Message");// @todo
        }
    }

    /**
     * This helper method verifies that the user really wants to save their
     * unsaved work, which they might not want to do. Note that it could be used
     * in multiple contexts before doing other actions, like creating a new
     * pose, or opening another pose, or exiting. Note that the user will be
     * presented with 3 options: YES, NO, and CANCEL. YES means the user wants
     * to save their work and continue the other action (we return true to
     * denote this), NO means don't save the work but continue with the other
     * action (true is returned), CANCEL means don't save the work and don't
     * continue with the other action (false is retuned).
     *
     * @return true if the user presses the YES option to save, true if the user
     * presses the NO option to not save, false if the user presses the CANCEL
     * option to not continue.
     */
    private boolean promptToSave() throws IOException {
        // PROMPT THE USER TO SAVE UNSAVED WORK
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        
        Alert alert = new Alert(AlertType.CONFIRMATION);
        alert.setTitle(props.getProperty(SAVE_PROMPT_TITLE));
        alert.setHeaderText(props.getProperty(SAVE_PROMPT_HEADER));
        alert.setContentText(props.getProperty(SAVE_PROMPT));

        ButtonType yesButton = new ButtonType("Yes");
        ButtonType noButton = new ButtonType("No");
        ButtonType cancelButton = new ButtonType("Cancel", ButtonData.CANCEL_CLOSE);

        alert.getButtonTypes().setAll(yesButton, noButton, cancelButton);

        Optional<ButtonType> saveWork = alert.showAndWait();

        // IF THE USER SAID YES, THEN SAVE BEFORE MOVING ON
        if (saveWork.get() == yesButton) {
            SlideShowModel slideShow = ui.getSlideShow();
            slideShowIO.saveSlideShow(slideShow);
            saved = true;
        } // IF THE USER SAID CANCEL, THEN WE'LL TELL WHOEVER
        // CALLED THIS THAT THE USER IS NOT INTERESTED ANYMORE
        else if (saveWork.get() == cancelButton) {
            return false;
        }

        // IF THE USER SAID NO, WE JUST GO ON WITHOUT SAVING
        // BUT FOR BOTH YES AND NO WE DO WHATEVER THE USER
        // HAD IN MIND IN THE FIRST PLACE
        return true;
    }

    /**
     * This helper method asks the user for a file to open. The user-selected
     * file is then loaded and the GUI updated. Note that if the user cancels
     * the open process, nothing is done. If an error occurs loading the file, a
     * message is displayed, but nothing changes.
     */
    private void promptToOpen() {
        // AND NOW ASK THE USER FOR THE COURSE TO OPEN
        FileChooser slideShowFileChooser = new FileChooser();
        slideShowFileChooser.setInitialDirectory(new File(PATH_SLIDE_SHOWS));
        File selectedFile = slideShowFileChooser.showOpenDialog(ui.getWindow());

        // ONLY OPEN A NEW FILE IF THE USER SAYS OK
        if (selectedFile != null) {
            try {
		SlideShowModel slideShowToLoad = ui.getSlideShow();
                slideShowIO.loadSlideShow(slideShowToLoad, selectedFile.getAbsolutePath());
                ui.reloadSlideShowPane(slideShowToLoad);
                saved = true;
                ui.updateToolbarControls(saved);
            } catch (Exception e) {
                ErrorHandler eH = ui.getErrorHandler();
                eH.processError(ERROR_DATA_FILE_LOADING, "Error", "File Load");// @todo
                saved = true;
                handleNewSlideShowRequest();
            }
        }
    }

    /**
     * This mutator method marks the file as not saved, which means that when
     * the user wants to do a file-type operation, we should prompt the user to
     * save current work first. Note that this method should be called any time
     * the pose is changed in some way.
     */
    public void markFileAsNotSaved() {
        saved = false;
    }

    /**
     * Accessor method for checking to see if the current pose has been saved
     * since it was last editing. If the current file matches the pose data,
     * we'll return true, otherwise false.
     *
     * @return true if the current pose is saved to the file, false otherwise.
     */
    public boolean isSaved() {
        return saved;
    }
    
    public void incrementCurrentSlide() {
        currentSlide = (currentSlide + 1) % ui.getSlideShow().getSlides().size();
    }
    
    public void decrementCurrentSlide() {
        if(currentSlide == 0)
            currentSlide = ui.getSlideShow().getSlides().size() - 1;
        else
            currentSlide--;
    }
}