package ssm.model;

import javafx.scene.control.TextField;

/**
 * This class represents a single slide in a slide show.
 * 
 * @author McKilla Gorilla & Ahsan Qureshi
 */
public class Slide {
    String imageFileName;
    String imagePath;
    String caption;
    Boolean isSelected;
    TextField captionT;
     
    /**
     * Constructor, it initializes all slide data.
     * @param initImageFileName File name of the image.
     * 
     * @param initImagePath File path for the image.
     * 
     */
    public Slide(String initImageFileName, String initImagePath) {
	imageFileName = initImageFileName;
	imagePath = initImagePath;
        caption = "";
        isSelected = false;
        captionT = new TextField(caption);
    }
    
    // ACCESSOR METHODS
    public String getImageFileName() { return imageFileName; }
    public String getImagePath() { return imagePath; }
    public String getCaption() { return caption; }
    public Boolean getIsSelected() { return isSelected; }
    public TextField getTextField() { return captionT; }
    
    // MUTATOR METHODS
    public void setImageFileName(String initImageFileName) {
	imageFileName = initImageFileName;
    }
    
    public void setTextField(TextField t) {
        captionT = t;
    }
    
    public void setImagePath(String initImagePath) {
	imagePath = initImagePath;
    }
    
    public void setImage(String initPath, String initFileName) {
	imagePath = initPath;
	imageFileName = initFileName;
    }
    
    public void setCaption(String c) {
        caption = c;
    }
    
    public void setIsSelected(Boolean initIsSelected) {
        isSelected = initIsSelected;
    }
}
